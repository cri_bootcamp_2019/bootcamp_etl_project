"""This module contains all useful functions for requestiong Wikipedia API"""

import requests

URL_API_BASE = "https://fr.wikipedia.org/w/api.php"


def get_author_info_from_wikipedia(author_name):
    '''
        Get author detailed info from wikipedia API

        :param author_name:
        :type: author_nanme: string
        :return: json_response: response of wikipedia API
        :rtype: dict
    '''
    params = {
        'action': "query",
        'titles': author_name,
        'format': "json",
        'prop': 'extracts|categories',
        'explaintext': True,
        'exintro': True

    }

    # TODO : Request Wikipedia API using params and return json_response
    try:
        req = requests.get(url=URL_API_BASE, params=params)
        json_response = req.json()
    except:
        json_response = {}

    return json_response
