"""This module contains all useful functions for scraping Babelio website"""

import requests
import numpy as np
import pandas as pd
from bs4 import BeautifulSoup

from bootcamp_cri.api.api_wikipedia import get_author_info_from_wikipedia

URL_BOOK_BASE = 'https://www.babelio.com'
URL_BOOK_LISTE = '/livrespopulaires_debut.php?p={page_number}'


def get_html_from_link(page_link):
    '''
        Get HTML from web page and parse it.

        :param page_link: link of the webpage we want to scrap
        :type page_link: string
        :return: BeautifulSoup object (HTML parsed)
        :rtype: bs4.BeautifulSoup
    '''

    page = requests.get(page_link)
    soup = BeautifulSoup(page.content, 'html.parser')
    return soup


def extract_book_info(book_html):
    '''
        Extract book infos from URL BOOK HTML

        :param book_html: BeautifulSoup Element that contains book infos
        :type book_html: bs4.element.Tag
        :return:
            - book_links: link to the book page
            - book_title : title of the book
            - book_image_link: link to the image of the book
        :rtype: tuple(string, string, string)
    '''

    # TODO : Get book_link, book_title and book_image_link from book_html and return this tuple

    book_links = book_html['href']
    book_title = book_html.find('h2').text
    book_image_link = book_html.find('img')['src']

    return book_links, book_title, book_image_link


def extract_author_info(author_html):
    '''
        Extract author info from URL BOOK HTML

        :param author_html: BeautifulSoup Element that contains author info
        :type author_html: bs4.element.Tag
        :return:
            - author_links: link to the author page
            - author_name : name of the author
        :rtype: tuple(string, string)
    '''

    # TODO : Get author_links, author_name from author_html and return this tuple
    author_links = author_html['href']
    author_name = author_html.find('h3').text
    return author_links, author_name


def extract_rate_from_book_page(book_link):
    '''
        Extract rate from book details page

        :param book_link: link of the book we want to extract rate
        :type book_link: string
        :return: rate: rate of the book
        :rtype: float

    '''

    # TODO : get html of book page, get rate from parsed html and return rate as float or nan value if there is no rate.

    soup = get_html_from_link(URL_BOOK_BASE + book_link)
    try:
        rate = float(soup.find('span', {'itemprop': 'ratingValue'}).text)
    except:
        rate = np.nan
    return rate


def get_info_from_page(page_link):
    '''
        Get Info from Bebelio page that contains list of books

        :param page_link: link of the webpage we want to scrap
        :type: page_link: string
        :return info_list: list that contains book info (book_links,
        book_title, book_image) and author info (author_links, author_name)
        :rtype: List
    '''

    # TODO : get html from page_link, extract books from html (1), iterate over books and for each book, extract book
    #  info html and author info html, use functions to extract book info and author info and store all these
    #  information in a list (2) and return it
    #  Hints :
    #   (1) Analyze html code and use Beautiful soup function to find elements (take a look at html tag
    #       AND class attributes)
    #   (2) Append a tuple of all information for each book

    soup_books = get_html_from_link(page_link)
    l_books = soup_books.findAll('div', {'class', 'col col-2-4 list_livre'})
    info_list = []
    for b in l_books:
        b_info_html, a_info_html = b.findAll('a')
        book_links, book_title, book_image_link = extract_book_info(b_info_html)
        author_links, author_name = extract_author_info(a_info_html)
        info_list.append((book_links, book_title, book_image_link, author_links, author_name))
    return info_list


def collect_all_information_and_save(file_name):
    '''
        "Main function" that collects all information from scraping babelio and using wikipedia api:
            - get info from list pages (page 1, 2, 3 and 4)
            - get rate of each book
            - get author information from wikipedia API (bootcamp_cri.api.api_wikipedia)
        ,store all these information in a pandas dataframe with following columns :
            - links, title, image_link, author_link, author, rate
        and save it in csv file.

        :param file_name: name of the csv file
        :type file_name: string

    '''

    # TODO : Iterate over pages, get information for each page and store it in a list and then create a dataframe with
    #  these information then extract rate and put it in a column and finally save the dataframe in a CSV file
    information_list = []
    for page_number in range(1, 2):
        information_list.extend(get_info_from_page(URL_BOOK_BASE + URL_BOOK_LISTE.format(page_number=page_number)))

    df = pd.DataFrame(information_list, columns=['links', 'title', 'image_link', 'author_link', 'author'])
    df['rate'] = df['links'].apply(extract_rate_from_book_page)
    df['author_information'] = df['author'].apply(get_author_info_from_wikipedia)
    df.to_csv(file_name, index=False)

#if __name__ == "__main__":
#    collect_all_information_and_save('scraping_and_api.csv')
